from FastaFile import FastaFile


class Seqs:


    def __init__(self):

        self.numseqs = 0
        self.name_list = []
        self.data_list = []


    def __len__(self):
        try:
            return int(round(float(sum(map(len, self.data_list)))
                             / self.numseqs))
        except:
            return 0


    def __repr__(self):
        return "%s(numseqs %d, len %d)" % (self.__class__.__name__,
                                           self.numseqs, len(self))


    def fromFastaFile(cls, fasta):

        seqs = cls()
        seqs.numseqs = fasta.numseqs

        for name in fasta.header_list:
            seqs.name_list.append(name)

        for data in fasta.data_list:
            seqs.data_list.append(data)

        return seqs

    fromFastaFile = classmethod(fromFastaFile)
