from sets import Set

from FastaFile import FastaFile
from TeiresiasPatterns import TeiresiasPatterns


class ResiduePairsException(Exception):
    pass


class ResiduePairs:


    def __init__(self, TeiresiasPatterns=None, FastaFile=None):

        self.patterns = TeiresiasPatterns
        self.fasta = FastaFile


    def compute_all(self, seqnum1, seqnum2):

        all_dict = {}
        seq_list = self.fasta.data_list
        patterns = self.patterns

        for pos, length in zip(patterns.pos_list, patterns.len_list):
##             if seqnum1 in pos and seqnum2 in pos:
            if Set([seqnum1, seqnum2]) <= (Set(pos)):
                for offset in range(length):
                    # i, j denote residue positions
                    # TODO: change code below - it may fail
                    # pos should be called offset_dict and seqnum may point to
                    # pos_array; maybe: create class derived from dict, put
                    # acces code in there; find other broken code
                    i = pos[seqnum1] + offset
                    j = pos[seqnum2] + offset
##                     if i not in all_dict:
##                         all_dict[i] = [ j ]
##                     else:
##                         all_list = all_dict[i]
##                         all_list.append(j)
##                         all_dict[i] = all_list
                    all_list = all_dict.get(i, [])
                    all_list.append(j)
                    all_dict[i] = all_list

        return all_dict


    def compute_all2(self, seqnum1, seqnum2):

        all1_dict = {}
        all2_dict = {}
        seq_list = self.fasta.data_list
        patterns = self.patterns

        for pos, length in zip(patterns.pos_list, patterns.len_list):
##             if seqnum1 in pos and seqnum2 in pos:
            if Set([seqnum1, seqnum2]) <= (Set(pos)):
                for offset in range(length):
                    # i, j denote residue positions
                    # TODO: change code below - it may fail
                    # pos should be called offset_dict and seqnum may point to
                    # array_list; maybe: create class derived from dict, put
                    # acces code in there; find other broken code
                    i = pos[seqnum1] + offset
                    j = pos[seqnum2] + offset

##                     if i not in all1_dict:
##                         all1_dict[i] = [ j ]
##                     else:
##                         all_list = all1_dict[i]
##                         all_list.append(j)
##                         all1_dict[i] = all_list
                    all_list = all1_dict.get(i, [])
                    all_list.append(j)
                    all1_dict[i] = all_list

##                     if j not in all2_dict:
##                         all2_dict[j] = [ i ]
##                     else:
##                         all_list = all2_dict[j]
##                         all_list.append(i)
##                         all2_dict[j] = all_list
                    all_list = all2_dict.get(j, [])
                    all_list.append(i)
                    all2_dict[j] = all_list

        return all1_dict, all2_dict


    def compute_compatible_from_all2(all1_dict, all2_dict):

        compat_dict = {}

        # remove residue pairs with 1:many relationship
        for i, all_list in all1_dict.iteritems():
            # remove duplicates
            all_set = Set(all_list)
            # enter compatible positions
            if len(all_set) == 1:
                compat_dict[i] = list(all_set)#[0] # do we want int?

        # remove residue pairs with many:1 relationship
        for j, all_list in all2_dict.iteritems():
            # remove duplicates
            all_set = Set(all_list)
            # remove non-compatible positions
            if len(all_set) > 1:
                for i in all_set:
                    try:
                        del compat_dict[i]
                    except KeyError:
                        pass

        return compat_dict

    compute_compatible_from_all2 = staticmethod(compute_compatible_from_all2)


    def compute_compatible(self, seqnum1, seqnum2):

        all1_dict, all2_dict = self.compute_all2(seqnum1, seqnum2)

        return ResiduePairs.compute_compatible_from_all2(all1_dict, all2_dict)


    def display(self, pairs_dict, fancy=True):

        if not fancy:
            print pairs_dict
        else:
            key_list = pairs_dict.keys()
            key_list.sort()
            for key in key_list:
                value_list = pairs_dict[key]
                value_list.sort()
                # need seqnum1, seqnum2 to access sequence data
                print key, ":", value_list
