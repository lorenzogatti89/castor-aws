import os
import tempfile

from DistMatrix import DistMatrix
from SeqUtils import SeqUtils


class PhylipFile:


    def __init__(self):
        self.interleaved = False

    def __len__(self):
        return len(self.data_list[0])


    def setInterleaved(self, interleaved=True):
        self.interleaved = interleaved


    def __get_repr_sequential(self):

        result = ''
        for name, data in zip(self.name_list, self.data_list):
            # format requires 10 chars: name + whitespace if necessary
            name = name[:10] + " " * (10 - len(name))
            # newline at beginning avoids unnecessary newline by 'print phylip'
            result +="\n%s %s" % (name, data)

        return result


    def __get_repr_interleaved(self):

        result = ''

        width = 60
        blank = ' ' * 10

        # part 1: names appear on left side
        for name, data in zip(self.name_list, self.data_list):
            # format requires 10 chars: name + whitespace if necessary
            name = name[:10] + " " * (10 - len(name))
            # newline at beginning avoids unnecessary newline by 'print phylip'
            result +="\n%s %s" % (name, data[:width])

        # part 2: left side is blank
        stop = width
        while stop < len(self):
            start = stop
            stop = start + width
            result += "\n"
            for  data in self.data_list:
                result +="\n%s %s" % (blank, data[start:stop])

        return result


    def __repr__(self):

        # format requires no. of seqs and alignment length preceded by spaces
        output = " %d %d" % (self.numseqs, len(self))
        if self.interleaved:
            output += self.__get_repr_interleaved()
        else:
            output += self.__get_repr_sequential()
        return output


    def fromAlign(cls, align):

        phylip = cls()
        # simple proxy
        phylip.numseqs = align.numseqs
        phylip.name_list = align.name_list
        phylip.data_list = align.data_list

        return phylip

    fromAlign = classmethod(fromAlign)


class Tool:

    INPUT_NAME = 'infile'
    OUTPUT_NAME = 'outfile'
    # each input line is a separate string, newline is appended automatically
    ACCEPT_OPTIONS = 'y'


    def get_inout_names(input_name=INPUT_NAME, output_name=OUTPUT_NAME):

        return input_name, output_name

    get_inout_names = staticmethod(get_inout_names)


    def prepare_tool(align, input_name=INPUT_NAME):

        phylip = PhylipFile.fromAlign(align)
        try:
            phy_file = file(input_name, 'w')
            print >> phy_file, phylip
        finally:
            phy_file.close()

    prepare_tool = staticmethod(prepare_tool)


    def run_tool(tool_name, tool_options=[], input_name=INPUT_NAME,
                 command_name='cmdfile'):
##                  command_name=Helper.COMMAND_NAME):

        try:
            cmd_file = file(command_name, 'w')

            if input_name != Tool.INPUT_NAME:
                cmd_file.write('%s\n' % input_name)

            tool_options += Tool.ACCEPT_OPTIONS
            for option in tool_options:
                cmd_file.write('%s\n' % option)
        finally:
            cmd_file.close()

        # delete all informational output (optional: could be saved)
        os.system('%s < %s > /dev/null' % (tool_name, command_name))

    run_tool = staticmethod(run_tool)


class Helper:

    COMMAND_NAME = 'cmdfile'
    DIR_PREFIX = 'phylip'


    def setup_tool_dir(dir_prefix=DIR_PREFIX):

        tempdir = tempfile.mkdtemp(prefix=dir_prefix)

        os.chdir(tempdir)

        return tempdir

    setup_tool_dir = staticmethod(setup_tool_dir)


    def cleanup_tool_dir(tempdir, input_name=Tool.INPUT_NAME,
                         output_name=Tool.OUTPUT_NAME,
                         command_name=COMMAND_NAME):

        os.remove(input_name)
        os.remove(output_name)
        os.remove(command_name)
        os.rmdir(tempdir)

    cleanup_tool_dir = staticmethod(cleanup_tool_dir)


class ProtDistMatrix(DistMatrix):

    TOOLNAME = DIR_PREFIX = 'protdist'


    def __init__(self, align, check=True):

        if check:
            SeqUtils.check_alignment(align)

        # prepare
        tempdir = Helper.setup_tool_dir(self.DIR_PREFIX)

        Tool.prepare_tool(align)
        Tool.run_tool(self.TOOLNAME)
        DistMatrix.__init__(self, filename=Tool.OUTPUT_NAME)

        # clean up
        Helper.cleanup_tool_dir(tempdir)
