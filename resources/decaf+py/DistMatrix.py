"""
This module contains functionality related to distance matrices.
"""


import re
import sys


class DistMatrix:
    """
    A matrix that stores distance values.

    Parsed formats include PHYLIP.  Undefined distances are denoted by '-'.
    """


    def __init__(self, filename=None, taxa_count=None, taxa_names=None):
        """
        Construct a new instance by reading a distance matrix file or
        by providing defaults for the specified number of taxa.

        PHYILIP and PDD (pattern-derived distances) format are
        recognized.
        """
        self.__name = []
        self.__row = []

        if filename is not None and taxa_count is not None:
            msg = 'only one parameter may be set'
            raise ValueError(msg)

        if filename is not None:
            self.__parse(filename)

        if taxa_count is not None:
            for i in range(taxa_count):
                self.__name.append(str(i+1))
                self.__row.append([])
                for j in range(taxa_count):
                    self[i].append(j)

        if taxa_names is not None:
            len1 = len(taxa_names)
            len2 = self.get_taxa_count()
            if len1 != len2:
                msg = 'number of taxa_names differs from get_taxa_count(): ' \
                      '%s != %s' % (len1, len2)
            for i, name in enumerate(taxa_names):
                self.__name[i] = name


    def fromiter(self, values):

        for row in self.__row:
            for i in range(len(row)):
                # modified from __parse()
                try:
                    row[i] = float(values.next())
                except ValueError:
                    # store undef value
                    row[i] = '-'


    def get_taxa_count(self):
        return len(self.__row)


    def get_names(self):
        return self.__name


    def get_rows(self):
        return self.__row


    def __getitem__(self, key):
        return self.__row[key]


    def __parse(self, filename):

        taxa_pattern = re.compile("^\s* \d+$", re.X)
        # [.\w]+ because \w+ fails to capture dots in names
        # \s* handles first taxa of PDD format (no space or number follows)
        line_pattern = re.compile("^( [.\-\w]+ ) \s* (.*)", re.X)
        cont_pattern = re.compile("^()          \s+ (.+)", re.X)

        for line in file(filename, 'rU'):

            line_match = taxa_pattern.search(line)
            if line_match != None:
                # skip to prevent reference error for __row later on
                continue

            # check for continuation line first
            line_match = cont_pattern.search(line)
            if line_match == None:
                # ...as line_pattern matches always
                line_match = line_pattern.search(line)
                if line_match != None:
                    name = line_match.group(1)
                    self.__name.append(name)
                    # line starts new row in matrix
                    self.__row.append([])

            # else-clause would give wrong results
            if line_match != None:
                values = line_match.group(2)
                number_list = values.split()
                i = self.get_taxa_count() - 1
                for number in number_list:
                    # self[i] is short for self.__row[i]
                    try:
                        self[i].append(float(number))
                    except ValueError:
                        # store undef value
                        self[i].append('-')


    def __str__(self):

        output = str(self.get_taxa_count())

        filled = self.is_completely_filled()

        for i, row in enumerate(self.__row):
            name = self.__name[i]
            # begin new line before name to avoid surplus one at end of matrix
            output += "\n" + name + " " * (10 - len(name))
            for col in row:
                cell = str(col)
                # each cell has width of 8, e.g. 0.123456
                output += " " + cell + " " * (8 - len(cell))

            # strip surplus whitespace at end of line but leave
            # whitespace-filled name untouched if matrix is in lower
            # triangular format
            if filled or i > 0:
                output = output.rstrip()

        return output


    def __iter__(self):
        for row in self.__row:
            for value in row:
                yield value


    def is_squared(self):
        """
        Test whether matrix is squared.
        """
        namecount = len(self.__name)
        for row in iter(self.__row):
            if len(row) > namecount:
                return False
        return True


    def is_completely_filled(self):
        """
        Test whether matrix every cell contains a value.  If matrix is
        not squared, MatrixNotSquaredError is raised.
        """
        namecount = len(self.__name)
        for row in iter(self.__row):
            if len(row) < namecount:
                return False
            elif len(row) > namecount:
                raise MatrixNotSquaredError, \
                      '%d entries in row but only %d taxa in matrix' \
                      % (len(row), namecount)
        return True


    def lowertriangular(self):
        """
        Convert matrix to lower triangular format.
        """
        if not self.is_completely_filled():
            # assume lower triangular format
            return

        for i, row in enumerate(self.__row):
            self.__row[i] = row[:i]


class MatrixNotSquaredError(Exception):
    """
    This error is raised when a row has more entries than there are
    taxa in the corresponding matrix.
    """
    pass
