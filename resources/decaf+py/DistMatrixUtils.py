"""
Functionality related to creating distances
"""


import os
from itertools import izip

from OutFile import OutFile
from Word import Distance
import DistMatrixFactory


def iter_distmats(seqs, vector, disttypes):

    for disttype in disttypes:
        dist = Distance(vector, disttype)
        distmat = DistMatrixFactory.create(seqs, dist)
        yield distmat


def write(iterdistmats, distnames, outdir=None, extension='.mat'):
    """
    Write distance matrices to files in a directory.

    If outdir is None, write to current working directory.
    """

    if outdir:
        os.makedirs(outdir)
    else:
        outdir = '.'

    for distmat, distname in izip(iterdistmats, distnames):
        fullname = os.path.join(outdir, distname+extension)
        OutFile(filename=fullname)
        print distmat
