"""
Functionality for PAML-related files
"""


class DatException(Exception):
    pass


class Dat:
    """
    Parse information in .dat files
    """


    def __init__(self, filename=None):

        self.equilibrium_frequencies = {}

        if filename is not None:
            self.equilibrium_frequencies = self.parse(filename)


    def parse(self, filename):

        amino_acids = 'ARNDCQEGHILKMFPSTWYV'
##         # default: equal frequencies
##         equilibrium_frequencies = {}.fromkeys(amino_acids,
##                                               1.0/len(amino_acids))
        # initialized to None, enables error detection
        equilibrium_frequencies = {}.fromkeys(amino_acids)

        # (state) .dat format:
        # (0) possible empty line(s)
        # (1) (lower) triangular matrix
        # (2) empty line(s)
        # (3) equilibrium frequencies
        #
        # not all .dat files contain equilibrium frequencies
        state = 0
        index = 0
        for line in file(filename):
            if state in [0, 2]:
                if len(line.split()) > 0:
                    state += 1
            if state == 1:
                if line.strip() == '':
                    state += 1
            if state == 3:
                if line.strip() == '':
                    break
                else:
                    values = map(float, line.split())
                    for freq in values:
                        aa = amino_acids[index]
                        equilibrium_frequencies[aa] = freq
                        index += 1

        if index != len(amino_acids):
            msg = 'read %i instead of %i frequencies' % (index,
                                                         len(amino_acids))
            raise DatException(msg)

        # real sequences may contain symbols from 'BZX?'
        # any amino acid
        equilibrium_frequencies['X'] = equilibrium_frequencies['?'] = 1.0
        # B = N or D
        equilibrium_frequencies['B'] = equilibrium_frequencies['N'] + \
                                       equilibrium_frequencies['D']
        # Z = Q or E
        equilibrium_frequencies['Z'] = equilibrium_frequencies['Q'] + \
                                       equilibrium_frequencies['E']

        return equilibrium_frequencies
