"""

Filter patterns - only the majority consensus of residue pair
combinations covered by patterns remains.

"""


from sets import Set

import PatternIter
import SeqUtils


class PatternFilter:


    def __init__(self, TeiresiasPatterns=None, FastaFile=None):

        self.patterns = TeiresiasPatterns
        self.fasta = FastaFile
        # relative or absolute
        self.set_majority()


    def set_majority(self, relative=True):

        if relative:
            self._collapse_majority = self._collapse_relative_majority
        else:
            self._collapse_majority = self._collapse_absolute_majority


    def _insert(self, d, respos1, respos2):

        d2 = d.get(respos1, {})
        count = d2.get(respos2, 0)
        d2[respos2] = count + 1
        d[respos1] = d2


    def _get_position_pairs(self, seqnum1, seqnum2):

        r1 = {}
        r2 = {}
        seqnum_list = [seqnum1, seqnum2]
        zipped = zip(self.patterns.pos_list, self.patterns.len_list)
        for offset_dict, length in zipped:
            if Set(seqnum_list) <= Set(offset_dict.keys()):
                cartprod = PatternIter.getcartprod_selection(offset_dict,
                                                             seqnum_list)
                for pw_info in cartprod:
                    # _ == seqnum
                    (_, pos1), (_, pos2) = pw_info
                    for pos_offset in range(length):
                        # residue positions
                        respos1 = pos1 + pos_offset
                        respos2 = pos2 + pos_offset
                        self._insert(r1, respos1, respos2)
                        self._insert(r2, respos2, respos1)

        return r1, r2


    def _collapse_absolute_majority(self, d):

        for respos1, d2 in d.items():
            ks, vs = d2.keys(), d2.values()
            if len(ks) == 1:
                d[respos1] = ks[0]
                continue
            total = sum(vs)
            majority = int(round(float(total+1)/2))
            # when no majority is found, the original dict remains
            for k, v in zip(ks, vs):
                if v >= majority:
                    d[respos1] = k
                    break


    def _collapse_relative_majority(self, d):

        for respos1, d2 in d.items():
            ks, vs = d2.keys(), d2.values()
            if len(ks) == 1:
                d[respos1] = ks[0]
                continue
            # when no majority is found, the original dict remains
            found = False
            k_max = 0
            v_max = 0
            for k, v in zip(ks, vs):
                if v > v_max:
                    found = True
                    k_max = k
                    v_max = v
                # another residue with that many paired residues
                elif v == v_max:
                    found = False
            if found:
                d[respos1] = k_max


    def _get_consistent_pairs(self, d1, d2):

        result = []

        ks, vs = d1.keys(), d1.values()
        for k in ks:
            v1 = d1[k]
            if isinstance(v1, int):
                v2 = d2[v1]
                # make sure both residue positions i and j point to each other
                if isinstance(v2, int) and v2 == k:
                    result.append((k, v1))

        return result


    def _show_pattern(self, seqnum1, seqnum2,
                      startpos1, startpos2, endpos1, endpos2):

        seq1 = self.fasta.data_list[seqnum1][startpos1:endpos1+1]
        seq2 = self.fasta.data_list[seqnum2][startpos2:endpos2+1]
        seqlet = ''
        for res1, res2, in zip(seq1, seq2):
            if res1 == res2:
                seqlet += res1
            else:
                seqlet += '.'
        print '2\t2\t%s %i %i %i %i' % (seqlet,
                                        seqnum1, startpos1, seqnum2, startpos2)


    def _show_patterns(self, pairs, seqnum1, seqnum2):

        pairs.sort()
        first_pattern = True
        for respos1, respos2 in pairs:
            if first_pattern:
                first_pattern = False
                startpos1 = respos1
                startpos2 = respos2
                endpos1 = respos1
                endpos2 = respos2
                continue
            if respos1-endpos1 != 1 or respos2-endpos2 != 1:
                # previous residue positions i and j end pattern
                self._show_pattern(seqnum1, seqnum2,
                                   startpos1, startpos2, endpos1, endpos2)
                startpos1 = respos1
                startpos2 = respos2
            endpos1 = respos1
            endpos2 = respos2
        try:
            # last pattern
            self._show_pattern(seqnum1, seqnum2,
                               startpos1, startpos2, endpos1, endpos2)
        # no pairs
        except UnboundLocalError:
            pass
            


    def majority_consensus_pairwise(self, seqnum1, seqnum2):

        p1, p2 = self._get_position_pairs(seqnum1, seqnum2)
        #print p1, p2
        self._collapse_majority(p1)
        self._collapse_majority(p2)
        #print p1, p2
        pairs = self._get_consistent_pairs(p1, p2)
        #print '# len =', len(pairs)
        #print pairs
        self._show_patterns(pairs, seqnum1, seqnum2)


    def majority_consensus(self):

        pw_iter = SeqUtils.iter_pairwise_combinations(self.fasta.numseqs)
        for i, j in pw_iter:
            #print '#', i, j
            self.majority_consensus_pairwise(i, j)
