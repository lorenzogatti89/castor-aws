#! /bin/env python


"""
PURPOSE:
compute word/k-mer distance from teiresias patterns:
wm distance

ASSUMPTION:

INPUT:
fasta file + blast similarity matrix

OUTPUT:
distance matrix
"""


import sys
from optparse import OptionParser, OptionGroup

from OutFile import OutFile
from FastaFile import FastaFile
from BlastMatrix import BlastMatrix
from Seqs import Seqs
import Wm
import DistMatrixFactory


def set_options():

    if len(sys.argv[1:]) == 0:
        sys.argv.append('-h')

    parser = OptionParser()

    group = OptionGroup(parser, "Mandatory Options")
    group.add_option("-f", "--fasta", metavar="FILE",
                     help="read sequence data from FILE")
    group.add_option("-m", "--matrix", metavar="FILE",
                     help="use blast similarity matrix from FILE in NCBI "
                     "format")
    parser.add_option_group(group)

    group = OptionGroup(parser, "Additional Options")
    group.add_option("-o", "--outfile", metavar="FILE",
                     help="write output to FILE")
    parser.add_option_group(group)

    return parser


def check_options(parser, options, args):

    if len(args) > 0:
        parser.error("non-option arguments not allowed")

    if not options.fasta or not options.matrix:
        parser.error("missing mandatory option(s)")


def main():

    parser = set_options()
    (options, args) = parser.parse_args()
    check_options(parser, options, args)

    OutFile(filename=options.outfile)

    fasta = FastaFile(filename=options.fasta)
    matrix = BlastMatrix(filename=options.matrix)
    seqs = Seqs.fromFastaFile(fasta)
    dist = Wm.Distance(seqs, matrix)
    distmat = DistMatrixFactory.create(seqs, dist)
    print distmat


# when executed, just run main():
if __name__ == '__main__':
    # Import Psyco if available
    try:
        import psyco
        psyco.full()
    except ImportError:
        pass
    main()
