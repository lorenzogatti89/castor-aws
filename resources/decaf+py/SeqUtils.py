import itertools

from FastaFile import FastaFile
from ResiduePairs import ResiduePairs


class SeqUtilsException(Exception):
    pass


class SeqUtils:


    def is_alignment(seqs):

        if seqs.numseqs < 2:
            return False

        len0 = len(seqs.data_list[0])

        if len0 == 0:
            return False

        for i in range(seqs.numseqs)[1:]:
            if len0 != len(seqs.data_list[i]):
                return False

        return True

    is_alignment = staticmethod(is_alignment)


    def check_alignment(seqs):

        try:
            aligned = SeqUtils.is_alignment(seqs)
            if not aligned:
                raise SeqUtilsException
        # must be Exception (not AttributeError) to catch SeqUtilsException
        except Exception, e:
            msg = "sequences are not aligned"
            # do we come from is_alignment()?
            if str(e):
                msg += " - seqs object?\n%s: %s" % (e.__class__.__name__, e)
            raise SeqUtilsException(msg)

    check_alignment = staticmethod(check_alignment)


    def _compute_percent_identity(seqdata1, seqdata2):

        id = 0
        for c1, c2 in itertools.izip(seqdata1, seqdata2):
            if c1 == c2:
                id += 1

        return 100.0 * id / len(seqdata1)

    _compute_percent_identity = staticmethod(_compute_percent_identity)


    def __compute_percent_identity_ResiduePairs(seqnum1, seqnum2, pairs,
                                                check_if_alignment):

        if check_if_alignment is not None:
            msg = "parameter 'check_if_alignment' must not be set for " \
                  "ResiduePairs"
            raise ValueError(msg)

        compat_dict = pairs.compute_compatible(seqnum1, seqnum2)

        seqdata1 = pairs.fasta.data_list[seqnum1]
        seqdata2 = pairs.fasta.data_list[seqnum2]

        seqlen = 0
        id = 0
        for pos1, [pos2] in compat_dict.iteritems():
            if seqdata1[pos1] == seqdata2[pos2]:
                id += 1
            seqlen += 1

        if seqlen == 0:
            pid = 0.0
        else:
            pid = 100.0 * id / seqlen

        return pid

    __compute_percent_identity_ResiduePairs = staticmethod(
        __compute_percent_identity_ResiduePairs)


    def __compute_percent_identity(seqnum1, seqnum2, alignment,
                                   check_if_alignment):

        if check_if_alignment:
            SeqUtils.check_alignment(alignment)

        seqdata1 = alignment.data_list[seqnum1]
        seqdata2 = alignment.data_list[seqnum2]

        return SeqUtils._compute_percent_identity(seqdata1, seqdata2)

    __compute_percent_identity = staticmethod(__compute_percent_identity)


    def compute_percent_identity(seqnum1, seqnum2, alignment,
                                 check_if_alignment=None):

        # check_if_alignment does not make sense for ResiduePairs

        if isinstance(alignment, ResiduePairs):
            method = SeqUtils.__compute_percent_identity_ResiduePairs
        else:
            method = SeqUtils.__compute_percent_identity

        return method(seqnum1, seqnum2, alignment, check_if_alignment)

    compute_percent_identity = staticmethod(compute_percent_identity)


    def __compute_percent_identity_list_ResiduePairs(pairs):

        percent_list = []

        for i, j in SeqUtils.iter_pairwise_combinations(pairs.fasta.numseqs):
            percent_list.append(
                SeqUtils.compute_percent_identity(i, j, pairs))

        return percent_list

    __compute_percent_identity_list_ResiduePairs = staticmethod(
        __compute_percent_identity_list_ResiduePairs)


    def __compute_percent_identity_list(alignment):

        SeqUtils.check_alignment(alignment)

        percent_list = []

        for i, j in SeqUtils.iter_pairwise_combinations(alignment.numseqs):
            percent_list.append(
                SeqUtils.compute_percent_identity(i, j, alignment,
                                                  check_if_alignment=False))

        return percent_list

    __compute_percent_identity_list = staticmethod(
        __compute_percent_identity_list)


    def compute_percent_identity_list(alignment):

        if isinstance(alignment, ResiduePairs):
            method = SeqUtils.__compute_percent_identity_list_ResiduePairs
        else:
            method = SeqUtils.__compute_percent_identity_list

        return method(alignment)

    compute_percent_identity_list = staticmethod(compute_percent_identity_list)


    # deprecated
    def iter_pairwise_combinations(numseqs, shape='lower'):

        if shape != 'lower':
            raise NotImplementedError

        for i in range(numseqs):
            for j in range(i):
                yield j, i

    iter_pairwise_combinations = staticmethod(iter_pairwise_combinations)


# right way to define this function
def iter_pairwise_combinations(numseqs, shape='lower'):

    if shape != 'lower':
        raise NotImplementedError

    for i in range(numseqs):
        for j in range(i):
            yield j, i
