#! /bin/env python


"""
PURPOSE:
compute many word/k-mer distances from teiresias patterns:
based on composition distance

ASSUMPTION:
teiresias patterns encode k-mers (teiresias option -s)

INPUT:
fasta file + teiresias files

OUTPUT:
distance matrices
"""


import sys

from optparse import OptionParser, OptionGroup

from FastaFile import FastaFile
from TeiresiasPatterns import TeiresiasPatterns
from Seqs import Seqs
from Word import Composition, Distance
import DistMatrixUtils


def set_options():

    if len(sys.argv[1:]) == 0:
        sys.argv.append('-h')

    parser = OptionParser()

    group = OptionGroup(parser, "Mandatory Options")
    group.add_option("-f", "--fasta", metavar="FILE",
                     help="read sequence names & lengths from FILE")
    group.add_option("--patterns", metavar="FILE",
                     help="read TEIRESIAS patterns from FILE (k-mers)")
    group.add_option("--patterns1", metavar="FILE",
                     help="read TEIRESIAS patterns from FILE ([k-1]-mers)")
    group.add_option("--patterns2", metavar="FILE",
                     help="read TEIRESIAS patterns from FILE ([k-2]-mers)")
    disttypes = Distance.get_disttypes_not_common()
    disttypes = "'%s'" % "', '".join(disttypes)
    group.add_option("-d", "--distances", metavar="METHOD", action="append",
                     help="choose one or more from: " + disttypes)
    parser.add_option_group(group)

    group = OptionGroup(parser, "Additional Options")
    group.add_option("-o", "--outdir", metavar="DIR",
                     help="write output to DIR")
    def_ext = '.mat'
    group.add_option("-e", "--extension", metavar="EXT", default=def_ext,
                     help="append EXT to filenames (default: '%s')" % def_ext)
    parser.add_option_group(group)

    return parser


def check_options(parser, options, args):

    if len(args) > 0:
        parser.error("non-option arguments not allowed")

    if not options.fasta or not options.patterns or not options.patterns1 \
           or not options.patterns2 or not options.distances:
        parser.error("missing mandatory option(s)")


def write_distmats(seqs, compos, disttypes, outdir, extension):

    try:
        distmats = DistMatrixUtils.iter_distmats(seqs, compos, disttypes)
        DistMatrixUtils.write(distmats, disttypes, outdir, extension)
    except Exception, e:
        sys.exit(e)


def main():

    parser = set_options()
    (options, args) = parser.parse_args()
    check_options(parser, options, args)

    fasta = FastaFile(filename=options.fasta)
    # use all (elementary) patterns
    patterns  = TeiresiasPatterns(filename=options.patterns, select_func=None)
    patterns1 = TeiresiasPatterns(filename=options.patterns1, select_func=None)
    patterns2 = TeiresiasPatterns(filename=options.patterns2, select_func=None)
    seqs = Seqs.fromFastaFile(fasta)
    compos = Composition(seqs, patterns, patterns1, patterns2,
                         counttype=Composition.OCCUR)
    write_distmats(seqs, compos, options.distances, options.outdir,
                   options.extension)


# when executed, just run main():
if __name__ == '__main__':
    # Import Psyco if available
    try:
        import psyco
        psyco.full()
    except ImportError:
        pass
    main()
