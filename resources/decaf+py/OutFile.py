import atexit
import sys


class OutFile:

    def __init__(self, filename=None, autoclose=True):

        self.oldfile = sys.stdout

        if filename:
            self.filename = filename
            sys.stdout = file(filename, 'w+')

        if autoclose:
            atexit.register(self.close)

    def close(self):
        if sys.stdout != self.oldfile:
            sys.stdout.close()
            sys.stdout = self.oldfile
