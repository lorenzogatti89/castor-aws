#! /bin/env python


"""
PURPOSE:
compute phylogenetic distances from teiresias patterns

INPUT:
fasta file + teiresias file

OUTPUT:
distance matrix in lower triangular format
"""


import sys

from optparse import OptionParser, OptionGroup

from OutFile import OutFile
from FastaFile import FastaFile
from TeiresiasPatterns import TeiresiasPatterns
from PatternSelect import PatternSelect
from PatternDist import PatternDist
from BlastMatrix import BlastMatrix


def set_options():

    if len(sys.argv[1:]) == 0:
        sys.argv.append('-h')

    parser = OptionParser()

    group = OptionGroup(parser, "Mandatory Options")
    group.add_option("-f", "--fasta", metavar="FILE",
                      help="read sequence data from FILE")
    group.add_option("-p", "--patterns", metavar="FILE",
                      help="read TEIRESIAS patterns from FILE")
    parser.add_option_group(group)

    group = OptionGroup(parser, "Additional Options")
    group.add_option("-o", "--outfile", metavar="FILE",
                      help="write output to FILE")
    parser.add_option_group(group)

    group = OptionGroup(parser, "Choosing Distances",
                        "Options that determine how distances are computed.")
    group.add_option("--protdist", action="store_true",
                     help="compute maximum likelihood distances by protdist")
    group.add_option("-m", "--matrix", metavar="FILE",
                     help="use blast similarity matrix from FILE in NCBI format")
    group.add_option("--pdistance", action="store_true",
                     help="compute simple p-distances")
    group.add_option("-c", "--correct", action="store_true",
                     help="apply Jukes Cantor correction to observed p-distance")
    group.add_option("-i", "--include", action="store_true",
                     help="include patterns which may violate the positivity "\
                     "property: by default they are not taken into account")
    parser.add_option_group(group)

    group = OptionGroup(parser, "Bootstrapping Distances",
                        "Options that affect sampling with replacement.")
    group.add_option("--bootstrap", metavar="INT", type="int",
                     help="set number of bootstraps to INT")
    group.add_option("--seed", metavar="INT", type="int",
                     help="initialize random noumber generator to INT")
    parser.add_option_group(group)

    return parser


def check_options(parser, options, args):

    if len(args) > 0:
        parser.error("non-option arguments not allowed")

    if not options.fasta or not options.patterns:
        parser.error("missing mandatory option(s)")

    if not options.protdist and not options.matrix and not options.pdistance:
        parser.error("choose a distance option:\n" \
                     "--protdist, --matrix, --pdistance")

    if (options.protdist and (options.matrix or options.pdistance) or
        options.matrix and (options.protdist or options.pdistance) or
        options.pdistance and (options.matrix or options.protdist)):
        parser.error("distance options exclude each other:\n" \
                     "--protdist, --matrix, --pdistance")

    if options.correct and not options.pdistance:
        parser.error("option --correct needs --pdistance")

    if options.bootstrap is not None and options.bootstrap < 1:
        parser.error("value of --bootstrap must be >= 1")

    if options.seed is not None and options.seed < 0:
        parser.error("value of --seed must be >= 0")

    if options.seed is not None and options.bootstrap is None:
        parser.error("option --seed requires --bootstrap")


def main():

    parser = set_options()
    options, args = parser.parse_args()
    check_options(parser, options, args)

    OutFile(filename=options.outfile)

    if options.include:
        ensure_positivity = None
    else:
        ensure_positivity = PatternSelect.is_selfdist_zero

    fasta = FastaFile(filename=options.fasta)
    patterns = TeiresiasPatterns(filename=options.patterns,
                                 select_func=ensure_positivity)
    if options.matrix is None:
        pdist = PatternDist(TeiresiasPatterns=patterns, FastaFile=fasta)
    else:
        matrix = BlastMatrix(filename=options.matrix)
        pdist = PatternDist(TeiresiasPatterns=patterns, FastaFile=fasta,
                            BlastMatrix=matrix)
    pdist.store_options(options)
    # more than one matrix if bootstrapped
    for matrix in pdist.iter_matrices():
        print matrix


# when executed, just run main():
if __name__ == '__main__':
    # Import Psyco if available
    try:
        import psyco
        psyco.full()
    except ImportError:
        pass
    main()
