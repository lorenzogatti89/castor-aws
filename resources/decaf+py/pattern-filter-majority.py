#! /bin/env python


"""
PURPOSE:
filter true positives from patterns

INPUT:
alignment in fasta file + teiresias file

OUTPUT:
statistics
"""


import sys

from optparse import OptionParser, OptionGroup

from OutFile import OutFile
from FastaFile import FastaFile
from TeiresiasPatterns import TeiresiasPatterns
from PatternSelect import PatternSelect
from PatternFilter import PatternFilter
from SeqUtils import SeqUtils


def set_options():

    if len(sys.argv[1:]) == 0:
        sys.argv.append('-h')

    parser = OptionParser()

    group = OptionGroup(parser, "Mandatory Options",
                        "Also, choose from Selection Options.")
    group.add_option("-f", "--fasta", metavar="FILE",
                     help="read sequence data from FILE")
    group.add_option("-p", "--patterns", metavar="FILE",
                     help="read TEIRESIAS patterns from FILE")
    parser.add_option_group(group)

    group = OptionGroup(parser, "Additional Options")
    group.add_option("-o", "--outfile", metavar="FILE",
                     help="write output to FILE")
    group.add_option("-n", "--no-include", action="store_true",
                     help="do not include patterns that occur more than once "\
                     "in any sequence")
    group.add_option("-a", "--absolute", action="store_true",
                     help="use absolute majority instead of relative")
    parser.add_option_group(group)

    return parser


def check_options(parser, options, args):

    if len(args) > 0:
        parser.error("non-option arguments not allowed")

    if not options.fasta or not options.patterns:
        parser.error("missing mandatory option")


def main():

    parser = set_options()
    options, args = parser.parse_args()
    check_options(parser, options, args)

    OutFile(filename=options.outfile)

    fasta = FastaFile(filename=options.fasta)

    if options.no_include:
        remove_patterns = PatternSelect.is_selfdist_zero
    else:
        remove_patterns = None

    patterns = TeiresiasPatterns(filename=options.patterns,
                                 select_func=remove_patterns)

    pattern_filter = PatternFilter(TeiresiasPatterns=patterns, FastaFile=fasta)
    pattern_filter.set_majority(not options.absolute)
    pattern_filter.majority_consensus()


# when executed, just run main():
if __name__ == '__main__':
    # Import Psyco if available
    try:
        import psyco
        psyco.full()
    except ImportError:
        pass
    main()
