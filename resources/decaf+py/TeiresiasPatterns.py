from array import array
import fileinput
import re


class TeiresiasPatternsException(Exception):
    pass


class TeiresiasPatterns:


    def __init__(self, filename=None, select_func=None):

        self.pattern_list = []     # pattern-strings incl. dots and brackets
        self.prob_list = []        # log likelihoods, if available
        self.supp_list = []        # no. of occurrences/pattern support
        self.seqs_list = []        # no. of distinct seqs a pattern occurs in
        self.pos_list = []         # position/offset of patterns in sequences
        self.len_list = []         # length of patterns

        self.filename = filename

        if filename != None:
            # maybe do .append() to be able to read multiple pattern files
            # and then compute distances
            # problem: has_probs may be different in different files
            self.has_probs, self.pattern_list, self.prob_list, \
                            self.supp_list, self.seqs_list, \
                            self.pos_list, self.len_list, \
                            = self.parse(filename, select_func)


    def contains_probabilities(filename):

        format1 = re.compile(
            r"""\d+ \t \d+ \t      # no. of occurrences, no. of sequences
            .*? \t                 # probability
            .*?                    # pattern: everything until next space
            (?:\s \d+ \s \d+)+     # at least one tuple: sequence, offset
            """, re.X)

        format2 = re.compile(
            r"""\d+ \t \d+ \t      # no. of occurrences, no. of sequences
            .*?                    # pattern: everything until next space
            (?:\s \d+ \s \d+)+     # at least one tuple: sequence, offset
            """, re.X)

        for line in fileinput.input(filename):

            match = format1.search(line)
            if match != None:
                fileinput.close()
                return True
            match = format2.search(line)
            if match != None:
                fileinput.close()
                return False

        fileinput.close()
        # no data in file, cannot determine presence/absence of probabilities
        return None

    contains_probabilities = staticmethod(contains_probabilities)


    def parse(filename, select_func=None):

        # save memory: store data in specialised arrays where possible
        pattern_list = []
        prob_list = array('d')
        supp_list = array('i')
        seqs_list = array('i')
        pos_list = []
        len_list = array('i')

        has_probs = TeiresiasPatterns.contains_probabilities(filename)

        if has_probs:
            format = re.compile(
            r"""(\d+) \t (\d+) \t  # no. of occurrences, no. of sequences
            (.*?) \t               # probability: actually log likelihood
            (.*?)                  # pattern: everything until next space
            ((?:\s \d+ \s \d+)+)   # at least one tuple: sequence, offset
            """, re.X)
            match_group_offset = 1
        else:
            format = re.compile(
            r"""(\d+) \t (\d+) \t  # no. of occurrences, no. of sequences
            (.*?)                  # pattern: everything until next space
            ((?:\s \d+ \s \d+)+)   # at least one tuple: sequence, offset
            """, re.X)
            match_group_offset = 0

        bracket = re.compile("  (  \[ .+? \]  )  ", re.X)   # match a bracket

        seq_offset = re.compile("\s (\d+) \s (\d+)", re.X)

        for line in fileinput.input(filename):

            match = format.search(line)
            if match != None:

                num_supp, num_seqs, pattern, list \
                          = match.group(1, 2,
                                        3 + match_group_offset,
                                        4 + match_group_offset)
                num_supp = int(num_supp)
                num_seqs = int(num_seqs)

                backbone = bracket.sub("1", pattern)
                length = len(backbone)

                offset_dict = {}
                for seq, offset in seq_offset.findall(list):
                    seq = int(seq)
                    offset = int(offset)
                    if seq not in offset_dict:
                        # save memory: store single position directly
                        offset_dict[seq] = offset
                    elif isinstance(offset_dict[seq], int):
                        stored_offset = offset_dict[seq]
                        # save memory: store 2 or more positions as array
                        array_list = array('i')
                        array_list.append(stored_offset)
                        array_list.append(offset)
                        offset_dict[seq] = array_list
                    else:
                        # further positions: simply append
                        offset_dict[seq].append(offset)

                if select_func == None or select_func(num_supp, num_seqs):
                    # add dataset if selected or no function supplied
                    pattern_list.append(pattern)
                    if (has_probs):
                        prob = float(match.group(3))
                        prob_list.append(prob)
                    supp_list.append(num_supp)
                    seqs_list.append(num_seqs)
                    pos_list.append(offset_dict)
                    len_list.append(length)

        return has_probs, pattern_list, prob_list, supp_list, seqs_list, \
               pos_list, len_list

    parse = staticmethod(parse)
