import array
import itertools2
import math
import random
import string
from copy import copy

from Align import Pairwise
from DistMatrix import DistMatrix
from Phylip import ProtDistMatrix
import SeqUtils


class PatternDistException(Exception):
    pass


class PatternDist:


    def __init__(self, TeiresiasPatterns, FastaFile, BlastMatrix=None):

        self.patterns = TeiresiasPatterns
        self.fasta = FastaFile
        self.blast_matrix = BlastMatrix

        self.bootstrap = None
        self.seed = None


    def store_options(self, options):

        self.correct_distance = options.correct
        self.protdist = options.protdist

        self.bootstrap = options.bootstrap
        self.seed = options.seed
        random.seed(self.seed)


    def get_lowermatrix(self, size, initvalue):

        matrix = []
        for i in range(size):
            matrix.append([])
            for j in range(i):
                matrix[i].append(initvalue)

        return matrix


    def get_seqlet(self, seqnum, pos, length):

        return self.fasta.data_list[seqnum][pos:pos+length]


    def get_differences(seqlet_i, seqlet_j):

        assert len(seqlet_i) == len(seqlet_j), "seqlets must have same length"

        diff = 0
        for pos in range(len(seqlet_i)):
            if seqlet_i[pos] != seqlet_j[pos]:
                diff += 1

        return diff

    get_differences = staticmethod(get_differences)


    def get_distance(seqlet_i, seqlet_j, BlastMatrix):

        assert len(seqlet_i) == len(seqlet_j), "seqlets must have same length"

        dist = 0
        for pos in range(len(seqlet_i)):
            aa1 = string.upper(seqlet_i[pos])
            aa2 = string.upper(seqlet_j[pos])
            # skip if aa1 == aa2 because dist == 0
            # check with BlastMatrix.dist_has_positivity()
            if aa1 == aa2:
                continue
            value = BlastMatrix.dist_dict[aa1+aa2]
            dist += value

        return dist

    get_distance = staticmethod(get_distance)


    def store_pairwise_data(self, pairwise_matrix, i, j, dist, length):

        olddist, oldlength = pairwise_matrix[i][j]
        pairwise_matrix[i][j] = olddist+dist, oldlength+length


    def compute_pairwise_matrix(self):

        patnums = len(self.patterns.pattern_list)
        if self.bootstrap is None:
            iter_patnums = xrange(patnums)
        else:
            iter_patnums = itertools2.repeatfunc(random.randint, patnums,
                                                 0, patnums-1)

        pos_list = self.patterns.pos_list
        len_list = self.patterns.len_list

        get_seqlet = self.get_seqlet
        if self.blast_matrix is None:
            # unit: differences per base in patterns
            get_dist = self.get_differences
        else:
            # unit: ?
            get_dist = lambda s_i, s_j: self.get_distance(s_i, s_j,
                                                          self.blast_matrix)

        # set (distance, length) to (0,0)
        pairwise_matrix = self.get_lowermatrix(self.fasta.numseqs, (0,0))
        for patnum in iter_patnums:
            pattern = self.patterns.pattern_list[patnum]

            offset_dict = pos_list[patnum]
            seq_list = offset_dict.keys()
            seq_list.sort()
            length = len_list[patnum]

            for j, seq_j in enumerate(seq_list):
                if isinstance(offset_dict[seq_j], int):
                    pos_j_list = [ offset_dict[seq_j] ]
                else:
                    pos_j_list = offset_dict[seq_j]
                for i, seq_i in enumerate(seq_list[j+1:]):
                    if isinstance(offset_dict[seq_i], int):
                        pos_i_list = [ offset_dict[seq_i] ]
                    else:
                        pos_i_list = offset_dict[seq_i]
                    for pos_i in pos_i_list:
                        for pos_j in pos_j_list:
                            seqlet_i = get_seqlet(seq_i, pos_i, length)
                            seqlet_j = get_seqlet(seq_j, pos_j, length)

                            dist = get_dist(seqlet_i, seqlet_j)
                            self.store_pairwise_data(pairwise_matrix,
                                                     seq_i, seq_j,
                                                     dist, length)

        return pairwise_matrix


    def fill_distance_matrix(self, distmat, pairwise_matrix):

        b = float(19) / 20 # == 0.95

        for j, i in SeqUtils.iter_pairwise_combinations(self.fasta.numseqs):
            dist, length = pairwise_matrix[i][j]
            if length > 0:
                dist = float(dist) / length
                if self.correct_distance:
                    # dist_corr = -b * log(1 - diff_obs/b)
                    dist = -b * math.log(1 - dist/b)
                distmat[i][j] = dist
            else:
                distmat[i][j] = '-'


    def get_align(self, all_i, all_j):

        align = Pairwise()
        align.numseqs = 2
        align.data_list.append(all_i)
        align.data_list.append(all_j)
        align.name_list.append('all_i')
        align.name_list.append('all_j')

        return align


    def get_protdist(self, align):

        matrix = ProtDistMatrix(align)
        matrix.lowertriangular()
        # there must be exactly one distance in matrix: assign it to dist
        for dist in matrix:
            pass

        return dist


    def compute_pairwise_protdist(self, seqnum_i, seqnum_j):

        all_i = ''
        all_j = ''

        patnums = len(self.patterns.pattern_list)
        if self.bootstrap is None:
            iter_patnums = xrange(patnums)
        else:
            iter_patnums = itertools2.repeatfunc(random.randint, patnums,
                                                 0, patnums-1)

        pos_list = self.patterns.pos_list
        len_list = self.patterns.len_list

        for patnum in iter_patnums:
            pattern = self.patterns.pattern_list[patnum]
            offset_dict = pos_list[patnum]
            seq_list = offset_dict.keys()
            seq_list.sort()
            length = len_list[patnum]
            for j, seq_j in enumerate(seq_list):
                if seq_j != seqnum_j:
                    continue
                if isinstance(offset_dict[seq_j], int):
                    pos_j_list = [ offset_dict[seq_j] ]
                else:
                    pos_j_list = offset_dict[seq_j]
                for i, seq_i in enumerate(seq_list[j+1:]):
                    if seq_i != seqnum_i:
                        continue
                    if isinstance(offset_dict[seq_i], int):
                        pos_i_list = [ offset_dict[seq_i] ]
                    else:
                        pos_i_list = offset_dict[seq_i]

                    for pos_i in pos_i_list:
                        for pos_j in pos_j_list:
                            seqlet_i = self.get_seqlet(seq_i, pos_i, length)
                            seqlet_j = self.get_seqlet(seq_j, pos_j, length)
                            all_i += seqlet_i
                            all_j += seqlet_j

        if len(all_i) > 0:
            align = self.get_align(all_i, all_j)
            dist = self.get_protdist(align)
        else:
            dist = '-'

        return dist


    def fill_distance_matrix_protdist(self, distmat):

        # ensure all iter_patnums use same sequence of random numbers
        if self.bootstrap is not None:
            rand_state = random.getstate()

        # save memory: loop over all pairs, combine patterns, call protdist
        for j, i in SeqUtils.iter_pairwise_combinations(self.fasta.numseqs):
            if self.bootstrap is not None:
                random.setstate(rand_state)
            dist = self.compute_pairwise_protdist(i, j)
            distmat[i][j] = dist


    def iter_matrices(self):

        names = self.fasta.header_list
        matrix_template = DistMatrix(taxa_count=len(names), taxa_names=names)
        matrix_template.lowertriangular()

        repeats = self.bootstrap
        if repeats is None:
            repeats = 1

        for _ in range(repeats):
            matrix = copy(matrix_template)
            if self.protdist:
                self.fill_distance_matrix_protdist(matrix)
            else:
                # stores (sum_dist, sum_len)
                pairwise_matrix = self.compute_pairwise_matrix()
                self.fill_distance_matrix(matrix, pairwise_matrix)
            yield matrix
