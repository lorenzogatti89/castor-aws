#! /bin/env python


"""
PURPOSE:
compute ACS distance between sequences

ASSUMPTION:

INPUT:
fasta file

OUTPUT:
distance matrix
"""


import sys

from optparse import OptionParser, OptionGroup

from OutFile import OutFile
from FastaFile import FastaFile
from Seqs import Seqs
import ACS
import DistMatrixFactory


def set_options():

    if len(sys.argv[1:]) == 0:
        sys.argv.append('-h')

    parser = OptionParser()

    group = OptionGroup(parser, "Mandatory Options")
    group.add_option("-f", "--fasta", metavar="FILE",
                     help="read sequence data from FILE")
    parser.add_option_group(group)

    group = OptionGroup(parser, "Additional Options")
    group.add_option("-o", "--outfile", metavar="FILE",
                     help="write output to FILE")
    parser.add_option_group(group)

    return parser


def check_options(parser, options, args):

    if len(args) > 0:
        parser.error("non-option arguments not allowed")

    if not options.fasta:
        parser.error("missing mandatory option")


def main():

    parser = set_options()
    options, args = parser.parse_args()
    check_options(parser, options, args)

    OutFile(filename=options.outfile)

    fasta = FastaFile(filename=options.fasta)
    seqs = Seqs.fromFastaFile(fasta)
    dist = ACS.Distance(seqs)
    distmat = DistMatrixFactory.create(seqs, dist)
    print distmat


# when executed, just run main():
if __name__ == '__main__':
    main()
