"""
Functionality for computing distances between sequences based on the
Average Common Substring length
Burstein, Ulitsky, Tuller, and Chor, RECOMB 2005: p.283-295
"""


from math import log

from SuffixTree import SuffixTree

import Distance


class Distance(Distance.PairwiseBase):

    def __init__(self, seqs):

        self._seqs = seqs
        self._trees = []
        self.__build_trees()


    numseqs = property(lambda self: self._seqs.numseqs)


    def __build_trees(self):

        for seq in self._seqs.data_list:
            suftree = SuffixTree()
            suftree.add(seq, 0)
            self._trees.append(suftree)


    # suftree must contain 2nd sequence seq2
    def __acs_length(self, seq, seqlen, suftree):

        total = 0

        for i in range(seqlen):
            match_len, _, _ = suftree.match(seq[i:])
            total += match_len

        value = float(total) / seqlen
        return value


    # suftree must contain 2nd sequence seq2
    def __distance_raw(self, seq1, seq2, suftree):

        seqlen1 = len(seq1)
        seqlen2 = len(seq2)

        term1 = log(seqlen1, 10) / self.__acs_length(seq1, seqlen1, suftree)
        # correction term: ensures self-distance is zero
        term2 = log(seqlen2, 10) / self.__acs_length(seq2, seqlen2, suftree)

        value = term1 - term2
        return value


    def pairwise_distance(self, seqnum1, seqnum2):

        seqs = self._seqs.data_list
        seq1 = seqs[seqnum1]
        seq2 = seqs[seqnum2]
        suftree1 = self._trees[seqnum1]
        suftree2 = self._trees[seqnum2]

        dist1 = self.__distance_raw(seq1, seq2, suftree2)
        dist2 = self.__distance_raw(seq2, seq1, suftree1)

        value = (dist1 + dist2) / 2
        return value
