#! /bin/sh

if [ -z $3 ] ; then
    echo "Usage: extract-words.sh len fasta out"
    exit
fi

LEN=$1
CON=`expr $LEN - 1`
WID=$LEN

FASTA=$2
OUT=$3

teiresias_char -s -l$LEN -c$CON -w$WID -k1 -v -p -i$FASTA -o$OUT

exit 0
