"""
Functionality for words (aka k-mers), i.e. strings in sequences
"""


from array import array
from itertools import izip
from math import log, sqrt
from operator import mul

import Distance

try:
    from pygsl import rng
    PYGSL = True
except ImportError, _pygslimporterror:
    PYGSL = False
    import warnings
    msg = '\nmodule pygsl not present - cannot compute poisson probabilities'
    warnings.warn(msg)


def get_counts_occurrence(TeiresiasPatterns, seqnum):

    result = array('i')

    for patnum, offset_dict in enumerate(TeiresiasPatterns.pos_list):
        try:
            # array
            count = len(offset_dict[seqnum])
        except TypeError:
            # int
            count = 1
        except KeyError:
            count = 0
        result.append(count)

    return result

# alias
get_counts = get_counts_occurrence


def get_counts_recurrence(TeiresiasPatterns, seqnum):

    result = array('i')

    for patnum, offset_dict in enumerate(TeiresiasPatterns.pos_list):
        patlen = TeiresiasPatterns.len_list[patnum]
        try:
            # array
            # make a copy so we don't change original data
            pos_list = offset_dict[seqnum][:]
            count = len(pos_list)
            # success (no Exception), now we need to take care of overlaps
            #print TeiresiasPatterns.pattern_list[patnum], offset_dict[seqnum]
            for i in range(count-1):
                # if endpos_i + 1 > startpos_i+1, then patterns overlap
                while pos_list[i] + patlen > pos_list[i+1]:
                    count -= 1
                    del pos_list[i+1]
        except IndexError:
            # caused by pos_list[i+1] in while-condition
            pass
        except TypeError:
            # int
            count = 1
        except KeyError:
            count = 0
        result.append(count)

    return result


def get_freqs(counts, seqlen, patlen):

    result = array('d')

    total = seqlen - patlen + 1
    for count in counts:
        result.append(float(count) / total)

    return result


def get_expected(words, freqmodel, seqlen):
    """
    seqlen is the length of the sequence the words appear in
    """

    result = array('d')
    for word in words:
        result.append(freqmodel.E(word, seqlen))

    return result


def nopygsl(*args):

    raise _pygslimporterror


def needspygsl(func):

    if not PYGSL:
        return nopygsl
    else:
        return func


@needspygsl
def get_probs_poisson(recurrences, expected):

    result = array('d')

    for recur, E in izip(recurrences, expected):
         if recur > 0:
             prob = 1.0 - rng.poisson_pdf(recur - 1, E)
         else:
             prob = 1.0
         result.append(prob)

    return result


def arithmetric_mean(values):

    value = float(sum(values)) / len(values)

    return value


# avoids numerical over- and underflow at the cost of compute time
def geometric_mean(values):

    inv_len = 1.0 / len(values)
    root = lambda x: pow(x, inv_len)
    roots = map(root, values)
    value = reduce(mul, roots)

    return value


def get_pattern_lookup(TeiresiasPatterns):

    result = {}

    for patnum, pattern in enumerate(TeiresiasPatterns.pattern_list):
        result[pattern] = patnum

    return result


class NoFreqs:
    """
    Variance of words based on their overlap capabilities

    as in the paper by Gentleman and Mullin, Biometrics (1989): 45(1), p.35-52
    """


    def probabilities(self, word):
        """
        Calculates P_L, the probability of a word,
        and all probabilities P_k of its prefixes

        Needs to be implemented by subclasses
        """

        raise NotImplementedError


    def E(self, word, seq_len, word_len=None):
        """
        E(X) = np = nP_L
        """

        # compute only if necessary
        if word_len is None:
            word_len = len(word)

        max_num = seq_len - word_len + 1
        # p = P_L == last element
        np = max_num * self.probabilities(word)[-1]

        return np


    def overlap_capability(self, word):

        value = ''

        length = len(word)
        # remember: word[:i] stops *before* i
        for i in range(1, length):
            if word[0:i] == word[length-i:length]:
                value += '1'
            else:
                value += '0'
        # i == len(word) ==> overlap capability is 1 (word overlaps itself)
        value += '1'

        return value


    def var(self, word, seq_len, word_len=None, overlap_capability=None,
            word_probs=None):
        """
        seq_len : length of sequence in which word occurs
        word_len: length of word (may be pre-computed)
        overlap_capability: may be pre-computed
        word_probs        : may be pre-computed

        original formula:

        var(X) = np(1-np) + p^2(n-L)(n-L+1)
                 + 2p*sum_{k=1}^{min(L-1,n-1)}(n-k)Q_{L-k}(1/4)^k
        where X: frequency of occurrence of subsequence, i.e. word
              Q: overlap capability of word
              L: length of word
              M: length of sequence
              n = M-L+1 (maximum possible number of words)
              p = (1/4)^L
        assuming DNA sequence, alphabet size = 4

        In this implementation, the alphabet size is not assumed to be 4.

        For arbitrary symbol frequencies, substitute in original formula:
        P_L for p
        P_k for (1/4)^k
        """

        # compute only if necessary
        if word_len is None:
            word_len = len(word)
        if overlap_capability is None:
            overlap_capability = self.overlap_capability(word)
        if word_probs is None:
            word_probs = self.probabilities(word)

        # p = P_L == last element
        p = word_probs[-1]

        max_num = seq_len - word_len + 1
        # should be: min_term = min(word_len-1, max_num-1)
        #       but: sum_term uses range() which stops at min_term-1
        min_term = min(word_len, max_num)
        # should be: overlap_capability[word_len-k] == Q_{L-k}
        #       but: overlap_capability starts at 0, Q at 1
        # should be: word_probs[k] == P_k
        #       but: word_probs starts at 0, P_k at 1
        sum_term = [(max_num-k) * int(overlap_capability[word_len-k-1])
                    * word_probs[k-1]
                    for k in range(1, min_term)]
        np = max_num * p
        n_L = max_num - word_len
        value = np*(1-np) + pow(p, 2)*(n_L)*(n_L+1) + 2*p*sum(sum_term)

        return value


class EqualFreqs(NoFreqs):


    def __init__(self, alphabet_size):
        """
        alphabet_size: assumes equal frequencies for all symbols
        """

        self.alphabet_size = alphabet_size
        # assumption of equal frequencies
        self._avg_symbol_frequency = 1.0 / self.alphabet_size


    def probabilities(self, word):

        result = []
        value = 1.0

        # actual symbols are not needed
        for _ in word:
            value *= self._avg_symbol_frequency
            result.append(value)

        return result


class EquilibriumFreqs(NoFreqs):


    def __init__(self, equilibrium_frequencies):
        """
        equilibrium_frequencies: dict with key : value pairs denoting
                                 symbol : equilibrium_frequency
        """

        self._equilibrium_frequencies = equilibrium_frequencies


    def probabilities(self, word):

        result = []
        value = 1.0

        for symbol in word.upper():
            value *= self._equilibrium_frequencies[symbol]
            result.append(value)

        return result


class Counts:

    OCCUR = 'occur'
    RECUR = 'recur'


    def __init__(self, seqs, patterns, counttype=OCCUR):

        if counttype == self.OCCUR:
            get_counts_func = get_counts_occurrence
        elif counttype == self.RECUR:
            get_counts_func = get_counts_recurrence
        else:
            msg = 'unknown counttype "%s"' % counttype
            raise ValueError(msg)

        seqlen = []
        for seq in seqs.data_list:
            seqlen.append(len(seq))
        self.seqlen = seqlen

        # store len_list? generalizes class!
        # assume global pattern length
        self.patlen = patterns.len_list[0]
        self.pattern_list = patterns.pattern_list
        self.counttype = counttype

        data = []
        for seqnum in range(seqs.numseqs):
            counts = get_counts_func(patterns, seqnum)
            data.append(counts)
        self._data = data


    numseqs = property(lambda self: len(self._data))


    def __getitem__(self, seqnum):

        return self._data[seqnum]


    def __str__(self):

        result = str(self.__class__)
        for data in self._data:
            result += '\n' + str(list(data))

        return result


class Freqs(Counts):

    def __init__(self, seqs, patterns, counttype=Counts.OCCUR):

        Counts.__init__(self, seqs, patterns, counttype)
        self.__relative_freqs()


    def __relative_freqs(self):

        for seqnum in range(self.numseqs):
            seqlen = self.seqlen[seqnum]
            freqs = get_freqs(self[seqnum], seqlen, self.patlen)
            self._data[seqnum] = freqs


class FreqsStd(Freqs):
    """
    Used by Wu, Burke and Davison,
    Biometrics (1997), 53: p.1431-1439

    Also features a variant as implemented by Vinga in NASC toolbox
    """

    STD = 'std'
    STD_VINGA = 'std_vinga'


    def __init__(self, seqs, patterns, freqmodel, stdtype=STD):

        if stdtype == self.STD:
            std_func = lambda freq, var: freq / sqrt(var)
        elif stdtype == self.STD_VINGA:
            std_func = lambda freq, var: freq / var
        else:
            msg = 'unknown stdtype "%s"' % stdtype
            raise ValueError(msg)

        # standardization takes care of variance of occurrences
        Freqs.__init__(self, seqs, patterns, counttype=Counts.OCCUR)
        self.stdtype = stdtype
        self._freqmodel = freqmodel
        self.__standardize_freqs(std_func)


    def __overlaps(self):

        result = []

        for pattern in self.pattern_list:
            # assumption: all occurrences point to same pattern
            overlap = self._freqmodel.overlap_capability(pattern)
            result.append(overlap)

        return result


    def __standardize(self, freqs, seqlen, overlaps, std_func):

        for patnum, pattern in enumerate(self.pattern_list):
            freq = freqs[patnum]
            overlap = overlaps[patnum]
            # assumption: all occurrences point to same pattern
            var = self._freqmodel.var(pattern, seqlen, self.patlen, overlap)
            freq_std = std_func(freq, var)
            # replace frequencies
            freqs[patnum] = freq_std


    def __standardize_freqs(self, std_func):

        overlaps = self.__overlaps()
        for seqnum in range(self.numseqs):
            freqs = self[seqnum]
            seqlen = self.seqlen[seqnum]
            self.__standardize(freqs, seqlen, overlaps, std_func)


class ProbsPoisson(Counts):
    """
    Used by van Helden,
    Bioinformatics (2004), 20(3): p.399-406
    """

    def __init__(self, seqs, patterns, freqmodel):

        # poisson model assumes recurrences
        Counts.__init__(self, seqs, patterns, Counts.RECUR)

        self._freqmodel = freqmodel
        self.__poisson_probs()


    def __poisson_probs(self):

        for seqnum in range(self.numseqs):
            seqlen = self.seqlen[seqnum]
            expected = get_expected(self.pattern_list, self._freqmodel,
                                    seqlen)
            probs = get_probs_poisson(self[seqnum], expected)
            self._data[seqnum] = probs


class CommonCounts(Counts):

    def __init__(self, seqs, patterns, counttype=Counts.OCCUR):

         Counts.__init__(self, seqs, patterns, counttype)


    def common_data(self, seqnum1, seqnum2):

        result = map(min, self[seqnum1], self[seqnum2])

        return result


class CommonProbsPoisson(CommonCounts):

    def __init__(self, seqs, patterns, freqmodel):

         # poisson model assumes recurrences
         CommonCounts.__init__(self, seqs, patterns, Counts.RECUR)

         self._freqmodel = freqmodel


    def common_data(self, seqnum1, seqnum2):

        seqlen1 = self.seqlen[seqnum1]
        seqlen2 = self.seqlen[seqnum2]
        # use min() since common count uses min()
        seqlen = min(seqlen1, seqlen2)
        expected = get_expected(self.pattern_list, self._freqmodel, seqlen)

        counts = CommonCounts.common_data(self, seqnum1, seqnum2)
        probs = get_probs_poisson(counts, expected)
        square = lambda x: pow(x, 2)
        result = map(square, probs)

        return result


class Composition(Counts):
    """
    Used by Hao and Qi,
    J of Bioinformatics and Computational Biology (2004), 2(1): p.1-19

    Uses Markov chains of order k-2
    """

    def __init__(self, seqs, patterns, patterns1, patterns2,
                 counttype=Counts.OCCUR):

        Counts.__init__(self, seqs, patterns, counttype)
        self._counts1 = Counts(seqs, patterns1, counttype)
        self._counts2 = Counts(seqs, patterns2, counttype)

        self.__check_patlen()
        self._lookup1 = get_pattern_lookup(patterns1)
        self._lookup2 = get_pattern_lookup(patterns2)

        for seqnum in range(seqs.numseqs):
            seqlen = self.seqlen[seqnum]
            self.__composition(seqnum, seqlen)


    def __check_patlen(self):

        if self.patlen != self._counts1.patlen + 1 or \
           self.patlen != self._counts2.patlen + 2:

            msg = 'pattern lengths do not follow n, n-1, n-2'

            raise ValueError(msg)


    def __markov_chain_freqs(self, seqnum, seqlen, patnum, patlen, pattern):

        len1 = seqlen - patlen + 1
        len2 = seqlen - patlen + 2
        len3 = seqlen - patlen + 3

        f = float(self._data[seqnum][patnum])

        patternL = pattern[:-1]
        patternR = pattern[1:]
        patternLR = pattern[1:-1]
        patnumL = self._lookup1[patternL]
        patnumR = self._lookup1[patternR]
        patnumLR = self._lookup2[patternLR]
        fL = self._counts1[seqnum][patnumL]
        fR = self._counts1[seqnum][patnumR]
        fLR = self._counts2[seqnum][patnumLR]

        f0 = float(fL)*fR/fLR * (float(len1)*len3/pow(len2, 2))

        return f, f0


    def __composition(self, seqnum, seqlen):

        compos = array('d')

        patlen = self.patlen
        for patnum, pattern in enumerate(self.pattern_list):
            try:
                f, f0 = self.__markov_chain_freqs(seqnum, seqlen, patnum,
                                                  patlen, pattern)
                value = (f - f0) / f0
            except ZeroDivisionError:
                value = 0.0
            compos.append(value)
        self._data[seqnum] = compos


class VectorFactory:

    @classmethod
    def create(cls, vectortype, seqs, patterns, freqmodel=None):

        all = cls.get_vectortypes()
        if vectortype not in all:
            msg = 'unknown vectortype "%s"' % vectortype
            raise ValueError(msg)

        if freqmodel is None:
            needsmodel = cls.get_vectortypes_model()
            if vectortype in needsmodel:
                msg = 'missing freqmodel for vectortype "%s"' % vectortype
                raise ValueError(msg)
            Vector = eval(vectortype)
            vector = Vector(seqs, patterns)
        else:
            simple = cls.get_vectortypes_simple()
            if vectortype in simple:
                # better to notify user than to silently ignore
                msg = 'freqmodel not needed for vectortype "%s"' % vectortype
                raise ValueError(msg)
            if vectortype == 'FreqsStdVinga':
                vector = FreqsStd(seqs, patterns, freqmodel,
                                  stdtype=FreqsStd.STD_VINGA)
            else:
                Vector = eval(vectortype)
                vector = Vector(seqs, patterns, freqmodel)

        return vector


    @classmethod
    def get_vectortypes(cls):

        result = cls.get_vectortypes_simple()
        result.extend(cls.get_vectortypes_model())

        return result


    @staticmethod
    def get_vectortypes_simple():

        result = ['Counts', 'CommonCounts', 'Freqs']

        return result


    @staticmethod
    def get_vectortypes_model():

        result = ['FreqsStd', 'FreqsStdVinga', 'ProbsPoisson',
                  'CommonProbsPoisson']

        return result


class DistanceTypes:

    @classmethod
    def get_disttypes(cls):
        """
        Return complete list of all disttypes.
        """

        result = [attr.lower() for attr in dir(cls) if attr[0].isupper()]

        return result


class DistanceMixed(Distance.PairwiseBase, DistanceTypes):
    """
    Combines Distance.COMMON_PROBS_POISSON_xxx and Distance.DIFF_ABS_xxx

    Does not require vector data but operates directly on seqs, patterns
    and freqmodel.
    """

    ADD   = 'add'
    MULT  = 'mult'
    MULT1 = 'mult1'
    MULT2 = 'mult2'


    def __init__(self, seqs, patterns, freqmodel, disttype):

        import Word
        disttype_common = Word.Distance.COMMON_PROBS_POISSON_MULT
        if disttype == self.ADD:
            disttype_common = Word.Distance.COMMON_PROBS_POISSON_ADD
            disttype_diff_abs = Word.Distance.DIFF_ABS_ADD
        elif disttype == self.MULT:
            disttype_diff_abs = Word.Distance.DIFF_ABS_MULT
        elif disttype == self.MULT1:
            disttype_diff_abs = Word.Distance.DIFF_ABS_MULT1
        elif disttype == self.MULT2:
            disttype_diff_abs = Word.Distance.DIFF_ABS_MULT2
        else:
            msg = 'unknown disttype "%s"' % disttype
            raise ValueError(msg)

        common = CommonProbsPoisson(seqs, patterns, freqmodel)
        self._common = Distance(common, disttype=disttype_common)
        probs = ProbsPoisson(seqs, patterns, freqmodel)
        self._diff_abs = Distance(probs, disttype=disttype_diff_abs)


    numseqs = property(lambda self: self._common.numseqs)


    def pairwise_distance(self, seqnum1, seqnum2):

        common = self._common.pairwise_distance(seqnum1, seqnum2)
        diff_abs = self._diff_abs.pairwise_distance(seqnum1, seqnum2)
        value = (common + diff_abs) / 2

        return value


class Distance(Distance.PairwiseBase, DistanceTypes):
    """
    Combines vector data with distance method.

    Requires CommonXXX vectors (with method common_data) for disttypes
    starting with 'common_'.  Other vectors cannot be combined with
    these disttypes.  This serves not only as a safety-mechanism to
    avoid AttributeErrors later on during actual distance computation,
    but also prevents wrong results.  E.g. when applying disttype
    DIFF_ABS_ADD to CommonProbsPoisson as opposed to ProbsPoisson.  In
    this case, results would be wrong because pairwise distances would
    access counts, not Poisson probabilities as these would be
    computed on-the-fly through method common_data; they are not
    stored in the ._data field of CommonProbsPoisson instances.
    """

    EUCLID_SQUARED            = 'euclid_squared'
    EUCLID_NORM               = 'euclid_norm'
    EUCLID_SEQLEN1            = 'euclid_seqlen1'
    EUCLID_SEQLEN2            = 'euclid_seqlen2'
    ANGLE_COS_DISS            = 'angle_cos_diss'
    ANGLE_COS_EVOL            = 'angle_cos_evol'
    COMMON_COUNT_FRACT        = 'common_count_fract'
    COMMON_PROBS_POISSON_ADD  = 'common_probs_poisson_add'
    COMMON_PROBS_POISSON_MULT = 'common_probs_poisson_mult'
    DIFF_ABS_ADD              = 'diff_abs_add'
    DIFF_ABS_MULT             = 'diff_abs_mult'
    DIFF_ABS_MULT1            = 'diff_abs_mult1'
    DIFF_ABS_MULT2            = 'diff_abs_mult2'


    def pwdist_euclid_squared(self, seqnum1, seqnum2):
        """
        Used by Blaisdell,
        Proceedings of the National Academy, USA (1986), 83: p.5155-5159
        """

        squared_diff = lambda x, y: pow(x - y, 2)
        value = sum(map(squared_diff, self[seqnum1], self[seqnum2]))

        return value


    def pwdist_euclid_norm(self, seqnum1, seqnum2):
        """
        A variant as implemented by Vinga in NASC toolbox
        """

        value = sqrt(self.pwdist_euclid_squared(seqnum1, seqnum2))

        return value


    def pwdist_euclid_seqlen1(self, seqnum1, seqnum2):

        seqlen = self._vector.seqlen
        value = float(self.pwdist_euclid_squared(seqnum1, seqnum2)) \
                / (float(seqlen[seqnum1] + seqlen[seqnum2]) / 2)

        return value


    def pwdist_euclid_seqlen2(self, seqnum1, seqnum2):

        seqlen = self._vector.seqlen
        squared_diff = lambda x, y: pow(float(x)/sqrt(seqlen[seqnum1]) -
                                        float(y)/sqrt(seqlen[seqnum2]), 2)
        value = sum(map(squared_diff, self[seqnum1], self[seqnum2]))

        return value


    def __angle_cos(self, seqnum1, seqnum2):

        nom = sum(map(mul, self[seqnum1], self[seqnum2]))
        square = lambda x: pow(x, 2)
        sum1 = sum(map(square, self[seqnum1]))
        sum2 = sum(map(square, self[seqnum2]))

        value = nom / (sqrt(sum1) * sqrt(sum2))

        return value


    def catchzerodivision(func):

        def wrapper(*args):
            try:
                value = func(*args)
            except ZeroDivisionError:
                value = '-'
            return value

        return wrapper
            

    @catchzerodivision
    def pwdist_angle_cos_diss(self, seqnum1, seqnum2):
        """
        Used by Hao and Qi,
        J of Bioinformatics and Computational Biology (2004), 2(1): p.1-19
        """

        value = (1 - self.__angle_cos(seqnum1, seqnum2)) / 2

        return value


    @catchzerodivision
    def pwdist_angle_cos_evol(self, seqnum1, seqnum2):
        """
        Used by Stuart, Moffett and Leader,
        Molecular Biology and Evolution (2002), 19(4): p.554-562
        """

        value = -log((1 + self.__angle_cos(seqnum1, seqnum2)) / 2)

        return value


    def pwdist_common_count_fract(self, seqnum1, seqnum2):
        """
        Used by Edgar,
        Nucleic Acids Research (2004), 32(1): p.380-385
        """

        total = sum(self._vector.common_data(seqnum1, seqnum2))
        seqlen = self._vector.seqlen
        min_len = min(seqlen[seqnum1], seqlen[seqnum2])
        min_total = min_len - self._vector.patlen + 1

        F = float(total) / min_total
        # Edgar, NAR (2004) says 0.1 but muscle3.52_src reads 0.02
        value = -log(0.1 + F)

        return value


    def pwdist_common_probs_poisson_add(self, seqnum1, seqnum2):
        """
        Used by van Helden,
        Bioinformatics (2004), 20(3): p.399-406
        """

        value = arithmetric_mean(self._vector.common_data(seqnum1, seqnum2))

        return value


    def pwdist_common_probs_poisson_mult(self, seqnum1, seqnum2):
        """
        Used by van Helden,
        Bioinformatics (2004), 20(3): p.399-406
        """

        value = geometric_mean(self._vector.common_data(seqnum1, seqnum2))

        return value


    def pwdist_diff_abs_add(self, seqnum1, seqnum2):
        """
        Used by van Helden,
        Bioinformatics (2004), 20(3): p.399-406
        """

        abs_diff = lambda x, y: abs(x - y)
        values = map(abs_diff, self[seqnum1], self[seqnum2])
        value = arithmetric_mean(values)

        return value


    def pwdist_diff_abs_mult(self, seqnum1, seqnum2):

        abs_diff = lambda x, y: abs(x - y)
        values = map(abs_diff, self[seqnum1], self[seqnum2])
        value = geometric_mean(values)

        return value


    def pwdist_diff_abs_mult1(self, seqnum1, seqnum2):

        abs_diff = lambda x, y: abs(x - y) or 1.0
        values = map(abs_diff, self[seqnum1], self[seqnum2])
        value = geometric_mean(values)

        return value


    def pwdist_diff_abs_mult2(self, seqnum1, seqnum2):

        abs_diff = lambda x, y: abs(x - y)
        values = map(abs_diff, self[seqnum1], self[seqnum2])
        filtered = filter(None, values)
        value = geometric_mean(filtered)

        return value


    def __init__(self, vector, disttype):

        try:
            pwdist_func = 'self.pwdist_%s' % disttype
            self.pairwise_distance = eval(pwdist_func)
        # method does not exist
        except AttributeError:
            msg = 'unknown disttype "%s"' % disttype
            raise ValueError(msg)

        if disttype.startswith('common_'):
            try:
                # if attribute does not exist
                method = vector.common_data
                import types
                # or it is not a method
                if not isinstance(method, types.MethodType):
                    raise AttributeError
            except AttributeError:
                # then we end up here
                msg = 'incompatible vector does not possess method ' \
                      '"common_data"'
                raise TypeError(msg)
        else:
            try:
                method = vector.common_data
                import types
                # if attribute is a method
                if isinstance(method, types.MethodType):
                    # then we end up here
                    msg = 'incompatible vector must not possess method ' \
                          '"common_data"'
                    raise TypeError(msg)
            except AttributeError:
                pass

        self._vector = vector
        self._disttype = disttype


    numseqs = property(lambda self: self._vector.numseqs)


    @classmethod
    def get_disttypes_not_common(cls):
        """
        Return all disttypes except those starting with 'common', i.e.
        working on all vectors with method pairwise_distance().
        """

        result = [attr.lower() for attr in dir(cls) if attr[0].isupper() and
                  not attr.startswith('COMMON')]

        return result


    @classmethod
    def get_disttypes_common(cls):
        """
        Return only those disttypes starting with 'common', i.e. requiring
        vectors with method common_data().
        """

        result = [attr.lower() for attr in dir(cls)
                  if attr.startswith('COMMON')]

        return result


    @classmethod
    def get_disttypes_euclid(cls):
        """
        Return only those disttypes starting with 'euclid', i.e. computing
        an Euclidean distance
        """

        result = [attr.lower() for attr in dir(cls)
                  if attr.startswith('EUCLID')]

        return result


    def __getitem__(self, seqnum):

        return self._vector[seqnum]
