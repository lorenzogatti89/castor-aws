import SeqUtils
from DistMatrix import DistMatrix
import Word


def iterdists(distance):

    for i, j in SeqUtils.iter_pairwise_combinations(distance.numseqs):
        value = distance.pairwise_distance(i, j)
        yield value


def create(seqs, distance):

    distmat = DistMatrix(taxa_count=seqs.numseqs, taxa_names=seqs.name_list)
    distmat.lowertriangular()
    distmat.fromiter(iterdists(distance))

    return distmat
