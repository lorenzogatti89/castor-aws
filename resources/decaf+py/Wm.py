"""
Functionality for computing distances between sequences based on the
W-metric
Vinga, Gouveia-Oliveira and Almeida, Bioinformatics (2004), 20(2): p.206-215
"""


import Distance


class Distance(Distance.PairwiseBase):

    def __init__(self, seqs, matrix):

        self._seqs = seqs
        self._matrix = matrix
        # last element is '*' (for BLOSUM at least): leave it out
        self._alphabet = matrix.alphabet_list[:-1]
        self._freqs = []
        self.__prepare()


    numseqs = property(lambda self: self._seqs.numseqs)


    def __prepare(self):

        seqs = self._seqs.data_list
        for seqnum in range(self._seqs.numseqs):
            counts = self.__get_alpha_counts(seqs[seqnum])
            freqs = self.__get_alpha_freqs(counts, len(seqs[seqnum]))
            self._freqs.append(freqs)


    def __get_alpha_dict(self):

        result = {}.fromkeys(self._alphabet, 0)

        return result


    def __get_alpha_counts(self, seq):

        result = self.__get_alpha_dict()

        for aa in seq.upper():
            try:
                result[aa] += 1
            except KeyError:
                msg = "sequence symbol '%s' does not appear in similarity " \
                      "matrix" % aa
                raise DistanceException(msg)

        return result


    def __get_alpha_freqs(self, alpha_counts, seqlen):

        result = {}

        for aa, count in alpha_counts.iteritems():
            result[aa] = float(count) / seqlen

        return result


    def pairwise_distance(self, seqnum1, seqnum2):

        aapairs = [(aa1, aa2)
                   for aa1 in self._alphabet
                   for aa2 in self._alphabet]
        weights = self._matrix.sim_dict
        freqs1 = self._freqs[seqnum1]
        freqs2 = self._freqs[seqnum2]
        value = 0.0
        for aa1, aa2 in aapairs:
            value += (weights[aa1+aa2]
                      * (freqs1[aa1] - freqs2[aa1])
                      * (freqs1[aa2] - freqs2[aa2]))

        return value
