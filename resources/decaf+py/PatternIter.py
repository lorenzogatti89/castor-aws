"""

Iterate over patterns.  Option pairwise controls whether patterns
occurring >= 3 times are reported as several pairwise patterns.

"""


def mult(seqnum, offset_list, cartprod):

    result = []

    if len(cartprod) == 0:
        for offset in offset_list:
            result.append([(seqnum, offset)])
    else:
        for offset in offset_list:
            for elem in cartprod:
                result.append(elem + [(seqnum, offset)])

    return result


def getcartprod(offset_dict):

    cartprod = []

    for seqnum in offset_dict.iterkeys():
        offset_list = offset_dict[seqnum]
        if isinstance(offset_list, int):
            offset_list = [ offset_list ]
        cartprod = mult(seqnum, offset_list, cartprod)

    return cartprod


def getcartprod_selection(offset_dict, seqnum_list):

    cartprod = []

    for seqnum in seqnum_list:
        offset_list = offset_dict[seqnum]
        if isinstance(offset_list, int):
            offset_list = [ offset_list ]
        cartprod = mult(seqnum, offset_list, cartprod)

    return cartprod


def getpairwise(patinfo):

    patinfo2 = []

    for i, elem_i in enumerate(patinfo):
        for j, elem_j in enumerate(patinfo[i+1:]):
            patinfo2.append([elem_i, elem_j])

    return patinfo2


def iterpatinfo_m(cartprod):

    for patinfo in cartprod:
        yield patinfo


def iterpatinfo_pw(cartprod):

    for patinfo in cartprod:
        for patinfo2 in getpairwise(patinfo):
            yield patinfo2


def iterpos(pos_list, pairwise):

    if pairwise:
        iterpatinfo = iterpatinfo_pw
    else:
        iterpatinfo = iterpatinfo_m

    for patnum, offset_dict in enumerate(pos_list):
        cartprod = getcartprod(offset_dict)
        for patinfo in iterpatinfo(cartprod):
            yield patnum, patinfo
