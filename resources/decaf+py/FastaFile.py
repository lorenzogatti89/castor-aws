import fileinput
import re


class FastaFileException(Exception):
    pass


class FastaFile:


    def __init__(self, filename=None):

        self.header_list = []
        self.data_list = []
        self.filename = filename

        if filename != None:
           self.header_list, self.data_list = self.parse(filename)

        self.numseqs = len(self.header_list)


    def parse(filename):

        header_list = []
        data_list = []

        count = -1

        # gets single block as required by phylip, final \s can match newline
        header_list_pattern = re.compile("^> \s* (.*?) \s", re.X)

        for line in fileinput.input(filename):
            match = header_list_pattern.search(line)
            if match != None:
                count += 1
                id = match.group(1)
                header_list.append(id + (" " * (10 - len(id))))
                # prepare data_list for else-clause
                data_list.append("")
            else:
                data_list[count] += line

        # TODO: does not work (data_list grows always even for empty lines)
##         if len(header_list) != len(data_list):
##             raise FastaFileException(
##                 'file "%s" contains different number of sequence names and '
##                 'data' % filename)
        if len(header_list) == 0:
            raise FastaFileException(
                'file "%s" does not contain sequence names' % filename)
##         if len(data_list) == 0:
##             raise FastaFileException(
##                 'file "%s" does not contain sequence data' % filename)

        for i in range(len(header_list)):
            data_list[i] = data_list[i].replace("\n", "")
            data_list[i] = data_list[i].replace("\r", "")

        return header_list, data_list

    parse = staticmethod(parse)
