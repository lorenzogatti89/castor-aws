from FastaFile import FastaFile
from SeqUtils import SeqUtils, SeqUtilsException


class Pairwise:


    def __init__(self):
        self.numseqs = 0
        self.name_list = []
        self.data_list = []


    def __len__(self):
        try:
            return len(self.data_list[0])
        except:
            return 0


    def __repr__(self):
        return "%s(numseqs %d, len %d)" % (self.__class__.__name__,
                                           self.numseqs, len(self))


    def get_pairwise(self):
        try:
            return [self.data_list[0], self.data_list[1]]
        except:
            return []


    # TODO: remove gap-only columns to obtain a 'proper' pairwise alignment


    def fromFastaFile(cls, fasta, seqnum1, seqnum2, proxy=True, check=False):

        # default parameters (proxy=True, check=False) create object quickly
        # ideal for looping over (and creating) many Pairwise objects

        if check:
            SeqUtils.check_alignment(fasta)

        align = cls()
        align.proxy = proxy
        align.check = check
        align.numseqs = 2

        if proxy:
            # same as not proxy
            align.name_list = [fasta.header_list[seqnum1],
                               fasta.header_list[seqnum2]]
            align.data_list = [fasta.data_list[seqnum1],
                               fasta.data_list[seqnum2]]
        else:
            name_list = []
            name_list.append(fasta.header_list[seqnum1])
            name_list.append(fasta.header_list[seqnum2])
            align.name_list = name_list
            data_list = []
            data_list.append(fasta.data_list[seqnum1])
            data_list.append(fasta.data_list[seqnum2])
            align.data_list = data_list

        return align

    fromFastaFile = classmethod(fromFastaFile)


class Multiple(Pairwise):


    def __init__(self):
        Pairwise.__init__(self)


    def __repr__(self):
        return "%s(numseqs %d, len %d)" % (self.__class__.__name__,
                                           self.numseqs, len(self))


    def get_pairwise(self, seqnum1, seqnum2):
        try:
            return [self.data_list[seqnum1], self.data_list[seqnum2]]
        except:
            return []


    def get_multiple(self):
        try:
            return self.data_list
        except:
            return []


    def fromFastaFile(cls, fasta, proxy=True, check=True):

        if check:
            SeqUtils.check_alignment(fasta)

        align = cls()
        align.proxy = proxy
        align.check = check
        align.numseqs = fasta.numseqs

        if proxy:
            align.name_list = fasta.header_list
            align.data_list = fasta.data_list
        else:
            name_list = []
            for name in fasta.header_list:
                name_list.append(name)
            align.name_list = name_list
            data_list = []
            for data in fasta.data_list:
                data_list.append(data)
            align.data_list = data_list

        return align

    fromFastaFile = classmethod(fromFastaFile)
