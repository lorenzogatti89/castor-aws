import re


class BlastMatrixException(Exception):
    pass


class BlastMatrix:


    def __init__(self, filename=None):

        self.alphabet_list = []
        self.sim_dict = {}
        self.dist_dict = {}

        if filename != None:
            self.alphabet_list, self.sim_dict = self.parse(filename)
            self.dist_dict = self.sim_to_dist()


    def parse(filename):

        sim_dict = {}

        # assume NCBI format:
        # ARNDCQEGHILKMFPSTWYVBZX*
        matrix = file(filename)
        for line in matrix:
            # find amino acid alphabet line
            # contains at least 2 spaces
            if re.match("  ", line):
                # cut off first and last element which is empty
                alphabet_list = re.split("\s+", line)[1:-1]
            # find data lines
            # contains symbol followed by at least 1 space
            if re.match("[ARNDCQEGHILKMFPSTWYVBZX\*] ", line, re.I):
                sim_list =  re.split("\s+", line)
                aa1 = sim_list[0]
                for i, aa2 in enumerate(alphabet_list):
                    value = int(sim_list[i+1])
                    sim_dict[aa1+aa2] = value
        matrix.close()

        return alphabet_list, sim_dict

    parse = staticmethod(parse)


    def sim_to_dist(self):

        dist_dict = {}

        # d(X,Y) = s(X,X) + s(Y,Y) - 2*s(X,Y)
        for aa1 in self.alphabet_list:
            score_aa1 = self.sim_dict[aa1+aa1]
            for aa2 in self.alphabet_list:
                score_aa2 = self.sim_dict[aa2+aa2]
                score_aa1_aa2 = self.sim_dict[aa1+aa2]
                dist_dict[aa1+aa2] = score_aa1 + score_aa2 - 2 * score_aa1_aa2

        return dist_dict


    def dist_has_positivity(self):

        for aa1 in self.alphabet_list:
            for aa2 in self.alphabet_list:
                if aa1 == aa2:
                    # check d(X,X) == 0
                    if self.dist_dict[aa1+aa2] != 0:
                        return False
                else:
                    # check d(X,Y) >= 0
                    if self.dist_dict[aa1+aa2] < 0:
                        return False

        return True


    def dist_has_triangle_inequality(self):

        s = self.sim_dict

        for aa1 in self.alphabet_list:
##             if aa1 in ['B', 'Z', 'X']:
##                 continue
            for aa2 in self.alphabet_list:
##                 if aa2 in ['B', 'Z', 'X']:
##                     continue
                for aa3 in self.alphabet_list:
##                     if aa3 in ['B', 'Z', 'X']:
##                         continue
                    if s[aa2+aa2] + s[aa1+aa3] < s[aa1+aa2] + s[aa2+aa3]:
##                         print aa1, aa2, aa3
                        return False

        return True
