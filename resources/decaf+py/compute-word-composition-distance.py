#! /bin/env python


"""
PURPOSE:
compute word/k-mer distance from teiresias patterns:
composition distance

ASSUMPTION:
teiresias patterns encode k-mers (teiresias option -s)

INPUT:
fasta file + teiresias files

OUTPUT:
distance matrix
"""


import sys

from optparse import OptionParser, OptionGroup

from OutFile import OutFile
from FastaFile import FastaFile
from TeiresiasPatterns import TeiresiasPatterns
from Seqs import Seqs
from Word import Composition, Distance
import DistMatrixFactory


def set_options():

    if len(sys.argv[1:]) == 0:
        sys.argv.append('-h')

    parser = OptionParser()

    group = OptionGroup(parser, "Mandatory Options")
    group.add_option("-f", "--fasta", metavar="FILE",
                     help="read sequence names & lengths from FILE")
    group.add_option("--patterns", metavar="FILE",
                     help="read TEIRESIAS patterns from FILE (k-mers)")
    group.add_option("--patterns1", metavar="FILE",
                     help="read TEIRESIAS patterns from FILE ([k-1]-mers)")
    group.add_option("--patterns2", metavar="FILE",
                     help="read TEIRESIAS patterns from FILE ([k-2]-mers)")
    parser.add_option_group(group)

    group = OptionGroup(parser, "Additional Options")
    group.add_option("-o", "--outfile", metavar="FILE",
                     help="write output to FILE")
    parser.add_option_group(group)

    return parser


def check_options(parser, options, args):

    if len(args) > 0:
        parser.error("non-option arguments not allowed")

    if not options.fasta or not options.patterns or \
           not options.patterns1 or not options.patterns2:
        parser.error("missing mandatory option(s)")


def main():

    parser = set_options()
    (options, args) = parser.parse_args()
    check_options(parser, options, args)

    OutFile(filename=options.outfile)

    fasta = FastaFile(filename=options.fasta)
    # use all (elementary) patterns
    patterns  = TeiresiasPatterns(filename=options.patterns, select_func=None)
    patterns1 = TeiresiasPatterns(filename=options.patterns1, select_func=None)
    patterns2 = TeiresiasPatterns(filename=options.patterns2, select_func=None)
    seqs = Seqs.fromFastaFile(fasta)
    compos = Composition(seqs, patterns, patterns1, patterns2,
                         counttype=Composition.OCCUR)
    dist = Distance(compos, Distance.ANGLE_COS_DISS)
    distmat = DistMatrixFactory.create(seqs, dist)
    print distmat


# when executed, just run main():
if __name__ == '__main__':
    # Import Psyco if available
    try:
        import psyco
        psyco.full()
    except ImportError:
        pass
    main()
