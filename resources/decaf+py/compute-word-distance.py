#! /bin/env python


"""
PURPOSE:
compute word/k-mer distance from teiresias patterns:
Euclidean or other distance

INPUT:
fasta file + teiresias file

ASSUMPTION:
teiresias patterns encode k-mers (teiresias option -s)

OUTPUT:
distance matrix
"""


import math
import sys
from optparse import OptionParser, OptionGroup

from OutFile import OutFile
from FastaFile import FastaFile
from TeiresiasPatterns import TeiresiasPatterns
from Seqs import Seqs
import Paml
from Word import EqualFreqs, EquilibriumFreqs, VectorFactory, Distance
import DistMatrixFactory


def set_options():

    if len(sys.argv[1:]) == 0:
        sys.argv.append('-h')

    parser = OptionParser()

    group = OptionGroup(parser, "Mandatory Options")
    group.add_option("-f", "--fasta", metavar="FILE",
                     help="read sequence data from FILE")
    group.add_option("-p", "--patterns", metavar="FILE",
                     help="read TEIRESIAS patterns from FILE (k-mers)")
    vectortypes = VectorFactory.get_vectortypes()
    def_vector = vectortypes[0] # Counts
    vectortypes = "'%s'" % "', '".join(vectortypes)
    group.add_option("-v", "--vector", metavar="DATA", default=def_vector,
                     help="default: '" + def_vector + "',   choose from: " +
                     vectortypes)
    # missing MixedDistance options
    disttypes = Distance.get_disttypes()
    def_dist = disttypes[-1] # euclid_squared
    disttypes = "'%s'" % "', '".join(disttypes)
    group.add_option("-d", "--distance", metavar="METHOD", default=def_dist,
                     help="default: '" + def_dist + "',   choose from: " +
                     disttypes)
    parser.add_option_group(group)

    group = OptionGroup(parser, "Frequency Model Options", "To use it, "
                        "specify either alphabet size or equilibrium "
                        "frequencies.  May be required depending on choice of "
                        "vector data.")
    group.add_option("-e", "--equilibrium", metavar="FILE",
                     help="read amino acid equilibrium frequencies from FILE  "
                     " (in paml .dat format)")
    group.add_option("-a", "--alphabet", metavar="SIZE", type=int,
                     help="set alphabet size to SIZE (assumes equal "
                     "frequencies)")
    parser.add_option_group(group)

    group = OptionGroup(parser, "Additional Options")
    group.add_option("-o", "--outfile", metavar="FILE",
                     help="write output to FILE")
    parser.add_option_group(group)

    return parser


def check_options(parser, options, args):

    if len(args) > 0:
        parser.error("non-option arguments not allowed")

    if not options.fasta or not options.patterns:
        parser.error("missing mandatory option(s)")

    if options.alphabet is not None and options.alphabet < 2:
        parser.error("value for option --alphabet too low")

    if options.alphabet is not None and options.equilibrium:
        parser.error("incompatible options")


def get_dist(seqs, patterns, freqmodel, vectortype, disttype):

    try:
        vector = VectorFactory.create(vectortype, seqs, patterns, freqmodel)
        dist = Distance(vector, disttype)
    except Exception, e:
        sys.exit(e)

    return dist


def main():

    parser = set_options()
    (options, args) = parser.parse_args()
    check_options(parser, options, args)

    OutFile(filename=options.outfile)

    fasta = FastaFile(filename=options.fasta)
    # use all (elementary) patterns
    patterns = TeiresiasPatterns(filename=options.patterns, select_func=None)
    if options.equilibrium is not None:
        dat = Paml.Dat(filename=options.equilibrium)
        freqmodel = EquilibriumFreqs(dat.equilibrium_frequencies)
    elif options.alphabet:
        freqmodel = EqualFreqs(alphabet_size=options.alphabet)
    else:
        freqmodel = None
    seqs = Seqs.fromFastaFile(fasta)
    dist = get_dist(seqs, patterns, freqmodel, options.vector,
                    options.distance)
    distmat = DistMatrixFactory.create(seqs, dist)
    print distmat


# when executed, just run main():
if __name__ == '__main__':
    # Import Psyco if available
    try:
        import psyco
        psyco.full()
    except ImportError:
        pass
    main()
