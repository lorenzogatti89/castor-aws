# castor-aws

This repository stores the configuration files and the source code to deploy *castor* web interface on AWS


### Deploy using AWS Cloudformation

    aws cloudformation deploy --template-file /path_to_template/cloudformation_autoscaling_conf.json --stack-name my-new-stack --parameter-overrides OperatorEMail=Value1 KeyName=Value2 InstanceType=c4.large
